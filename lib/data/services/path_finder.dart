import '../model/field.dart';
import '../model/result.dart';

abstract class PathFinder {
  Stream<double> get progressStream;

  Result findPath(Field field, {int iterator = 0});

  void dispose();
}
