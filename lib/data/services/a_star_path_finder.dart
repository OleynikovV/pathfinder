import 'dart:async';
import 'dart:math';

import 'package:collection/collection.dart';
import 'package:webspark_app/data/model/result.dart';

import '../model/field.dart';
import '../model/node.dart';
import 'path_finder.dart';

class AStarPathFinder implements PathFinder {
  final _progressController = StreamController<double>.broadcast();

  @override
  Stream<double> get progressStream => _progressController.stream;

  @override
  Result findPath(Field field, {int iterator = 0}) {
    final openSet = PriorityQueue<Node>((a, b) => a.f.compareTo(b.f));
    final closedSet = <Node>{};

    final start = field.start.copyWith(h: _getDistance(field.start, field.end));

    openSet.add(start);

    while (openSet.isNotEmpty) {
      final progress = closedSet.length / (field.grid.length * field.grid.length);
      _progressController.add(progress + iterator.toDouble());

      final currentNode = openSet.removeFirst();

      if (currentNode == field.end) {
        _progressController.add(1.0 + iterator.toDouble());

        final reconstructPath = _reconstructPath(start, currentNode);
        return Result(field, reconstructPath);
      }

      closedSet.add(currentNode);

      for (final neighbor in _getNeighbors(currentNode, field.grid)) {
        if (closedSet.contains(neighbor)) {
          continue;
        }

        final tempGScore = currentNode.g + _getDistance(currentNode, neighbor);

        if (tempGScore < neighbor.g) {
          openSet.remove(neighbor);
        }

        if (!openSet.contains(neighbor)) {
          openSet.add(neighbor.copyWith(
            g: tempGScore,
            h: _getDistance(neighbor, field.end),
            f: neighbor.g + neighbor.h,
            parent: currentNode,
          ));
        }
      }
    }

    _progressController.add(1.0 + iterator);
    return Result(field, []);
  }

  double _getDistance(Node a, Node b) => sqrt(
        pow(a.position.x - b.position.x, 2) + pow(a.position.y - b.position.y, 2),
      );

  final List<List<int>> _neighborDirections = const [
    [1, -1],
    [1, 0],
    [1, 1],
    [0, -1],
    [0, 1],
    [-1, -1],
    [-1, 0],
    [-1, 1],
  ];

  List<Node> _getNeighbors(Node current, List<List<String>> grid) {
    final neighbors = <Node>[];

    for (final direction in _neighborDirections) {
      final newX = current.position.x + direction[0];
      final newY = current.position.y + direction[1];

      if (newX >= 0 && newX < grid.length && newY >= 0 && newY < grid.length && grid[newX][newY] == '.') {
        neighbors.add(Node(Point(newX, newY), parent: current));
      }
    }

    return neighbors;
  }

  List<Node> _reconstructPath(Node startNode, Node endNode) {
    final path = <Node>[];
    Node currentNode = endNode;

    while (currentNode != startNode) {
      path.add(currentNode);
      currentNode = currentNode.parent!;
    }
    path.add(startNode);

    return path.reversed.toList();
  }

  @override
  void dispose() => _progressController.close();
}
