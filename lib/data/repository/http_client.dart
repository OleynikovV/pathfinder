abstract class HttpClient {
  Future<dynamic> executeGetRequest(String url);

  Future<dynamic> executePostRequest(String url, List<dynamic> data);
}
