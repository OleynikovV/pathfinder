import 'package:dio/dio.dart';

import 'http_client.dart';

class DioClient implements HttpClient {
  final Dio _dioClient;

  DioClient(this._dioClient) {
    _dioClient.options = BaseOptions(headers: {"Content-Type": "application/json"});
  }

  @override
  Future executeGetRequest(String url) async {
    try {
      final response = await _dioClient.get(url);
      return response.data;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future executePostRequest(String url, List<dynamic> data) async {
    try {
      return await _dioClient.post(url, data: data);
    } catch (e) {
      rethrow;
    }
  }
}
