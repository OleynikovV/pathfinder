import 'field.dart';
import 'node.dart';

class Result {
  final Field field;
  final List<Node> steps;
  late final String path;

  Result(this.field, this.steps) : path = _generatePath(steps);

  static String _generatePath(List<Node> steps) {
    if (steps.isEmpty) return '';
    return steps.map((step) => step.toString()).join('->');
  }

  Map<String, dynamic> toJson() => {
        'id': field.id,
        'result': {
          'steps': steps.map((step) => step.toJson()).toList(),
          'path': path,
        },
      };
}
