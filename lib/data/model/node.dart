import 'dart:math';

import 'package:equatable/equatable.dart';

class Node extends Equatable {
  final Point<int> position;
  final double g, h, f;
  final Node? parent;

  const Node(this.position, {this.g = 0, this.h = 0, this.f = 0, this.parent});

  Node copyWith({double? g, double? h, double? f, Node? parent}) => Node(
        position,
        g: g ?? this.g,
        h: h ?? this.h,
        f: f ?? this.f,
        parent: parent ?? this.parent,
      );

  factory Node.fromJson(Map<String, dynamic> json) => Node(
        Point<int>(json['y'] as int, json['x'] as int),
      );

  Map<String, dynamic> toJson() => {
        'x': position.y.toString(),
        'y': position.x.toString(),
      };

  @override
  List<Object?> get props => [position];

  @override
  String toString() => '(${position.y},${position.x})';
}
