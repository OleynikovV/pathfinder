import 'node.dart';

class Field {
  final String id;
  final Node start, end;
  final List<List<String>> grid;

  const Field(this.id, this.start, this.end, this.grid);

  factory Field.fromJson(Map<String, dynamic> json) => Field(
        json['id'] as String,
        Node.fromJson(json['start'] as Map<String, dynamic>),
        Node.fromJson(json['end'] as Map<String, dynamic>),
        (json['field'] as List).map<List<String>>((string) => string.split('')).toList(),
      );

  @override
  String toString() => 'start: $start,\nend: $end,\nfield:\n${grid.map((row) => row.join(' ')).join('\n')}';
}
