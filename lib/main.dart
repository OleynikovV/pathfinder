import 'package:flutter/material.dart';
import 'package:webspark_app/util/locator.dart' as locator;
import 'package:webspark_app/util/router.dart' as router;

import 'util/themes/themes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  locator.setup();

  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: defaultTheme,
      debugShowCheckedModeBanner: false,
      routerConfig: router.routerConfig,
    );
  }
}
