import '../../../data/model/result.dart';
import '../../../data/repository/http_client.dart';

class ResultSender {
  final HttpClient _client;

  ResultSender(this._client);

  Future<void> sendResults(String url, List<Result> resultList) async {
    try {
      final resultData = resultList.map((result) => result.toJson()).toList();
      final response = await _client.executePostRequest(url, resultData);

      if (response.data['error'] != false) {
        throw (Exception(response.data['message']));
      }
    } catch (e) {
      rethrow;
    }
  }
}
