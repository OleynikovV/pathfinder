part of 'process_bloc.dart';

sealed class ProcessEvent extends Equatable {
  const ProcessEvent();

  @override
  List<Object> get props => [];
}

class CalculateResultEvent extends ProcessEvent {
  final List<Field> fieldList;

  const CalculateResultEvent(this.fieldList);

  @override
  List<Object> get props => [fieldList];
}

class UpdateProgressEvent extends ProcessEvent {
  final double progress;

  const UpdateProgressEvent(this.progress);

  @override
  List<Object> get props => [progress];
}

class SendResultEvent extends ProcessEvent {
  final String url;

  const SendResultEvent(this.url);

  @override
  List<Object> get props => [url];
}
