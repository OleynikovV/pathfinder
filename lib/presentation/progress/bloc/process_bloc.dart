import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';

import '../../../data/model/field.dart';
import '../../../data/model/result.dart';
import '../../../data/services/path_finder.dart';
import '../repository/result_sender.dart';

part 'process_event.dart';
part 'process_state.dart';

class ProcessBloc extends Bloc<ProcessEvent, ProcessState> {
  final PathFinder _pathFinder;
  final ResultSender _resultSender;

  StreamSubscription<double>? _progressSubscription;

  ProcessBloc(this._pathFinder, this._resultSender) : super(const ProcessState()) {
    on<CalculateResultEvent>(_onCalculateResult);
    on<UpdateProgressEvent>(_onUpdateProgress);
    on<SendResultEvent>(_onSendResult);
  }

  _onCalculateResult(CalculateResultEvent event, Emitter<ProcessState> emit) async {
    emit(state.copyWith(
      status: ProcessStatus.calculating,
    ));

    await Future.delayed(const Duration(seconds: 1));

    _progressSubscription = _pathFinder.progressStream.listen((progress) {
      add(UpdateProgressEvent(progress / event.fieldList.length));
    });

    for (int i = 0; i < event.fieldList.length; i++) {
      final result = _pathFinder.findPath(event.fieldList[i], iterator: i);
      Logger().d(result.toJson());
      emit(state.copyWith(resultList: [...state.resultList, result]));
    }

    emit(state.copyWith(
      status: ProcessStatus.success,
    ));
  }

  _onUpdateProgress(UpdateProgressEvent event, Emitter<ProcessState> emit) async {
    Logger().d(event.progress);
    emit(state.copyWith(progress: event.progress));
  }

  _onSendResult(SendResultEvent event, Emitter<ProcessState> emit) async {
    emit(state.copyWith(
      status: ProcessStatus.loading,
    ));
    try {
      if (state.resultList.isNotEmpty) {
        await _resultSender.sendResults(event.url, state.resultList);
        emit(state.copyWith(
          status: ProcessStatus.sent,
        ));
      } else {
        emit(state.copyWith(
          status: ProcessStatus.error,
          errorMessage: 'Failed to send result. Result list is empty',
        ));
      }
    } catch (e) {
      emit(state.copyWith(
        status: ProcessStatus.error,
        errorMessage: 'Something went wrong while trying to send result',
      ));
    }
  }

  @override
  Future<void> close() {
    _progressSubscription?.cancel();
    _pathFinder.dispose();
    return super.close();
  }
}
