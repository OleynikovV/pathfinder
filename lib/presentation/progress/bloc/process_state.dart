part of 'process_bloc.dart';

enum ProcessStatus { initial, calculating, loading, success, sent, error }

class ProcessState extends Equatable {
  final ProcessStatus status;
  final String errorMessage;

  final double progress;
  final List<Result> resultList;

  const ProcessState({
    this.status = ProcessStatus.initial,
    this.errorMessage = 'Something went wrong',
    this.progress = 0,
    this.resultList = const [],
  });

  ProcessState copyWith({
    ProcessStatus? status,
    String? errorMessage,
    double? progress,
    List<Result>? resultList,
  }) =>
      ProcessState(
          status: status ?? this.status,
          errorMessage: errorMessage ?? this.errorMessage,
          progress: progress ?? this.progress,
          resultList: resultList ?? this.resultList);

  @override
  List<Object> get props => [status, errorMessage, progress, resultList];
}
