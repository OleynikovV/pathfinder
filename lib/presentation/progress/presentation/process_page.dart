import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../../data/model/field.dart';
import '../../../util/resources/edge_inserts.dart';
import '../../widgets/expanded_box.dart';
import '../../widgets/loading_indicator.dart';
import '../bloc/process_bloc.dart';

class ProcessPage extends StatelessWidget {
  final String url;
  final List<Field> fieldList;

  const ProcessPage(this.url, this.fieldList, {super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Process screen'),
      ),
      body: Padding(
        padding: edgeInsertsAll16,
        child: BlocConsumer<ProcessBloc, ProcessState>(
          listener: (context, state) {
            if (state.status == ProcessStatus.error) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.errorMessage),
                  backgroundColor: Colors.red,
                ),
              );
            }
            if (state.status == ProcessStatus.sent) {
              context.go('/field_fetch/result_list', extra: {
                'resultList': state.resultList,
              });
            }
          },
          builder: (context, state) {
            return Column(
              children: [
                expandedBox,
                _buildHeaderText(state.status),
                Text('${state.progress.toInt() * 100}%'),
                _buildProgressIndicator(state.progress),
                expandedBox,
                _buildSendButton(context, state.status),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildHeaderText(ProcessStatus status) {
    String headerText;
    switch (status) {
      case ProcessStatus.initial:
        headerText = 'Initializing service';
        break;
      case ProcessStatus.calculating:
        headerText = 'Calculating...';
        break;
      case ProcessStatus.loading:
        headerText = 'Sending request to the server';
        break;
      case ProcessStatus.success:
        headerText = 'All calculations has finished, you can send your results to server';
        break;
      case ProcessStatus.sent:
        headerText = 'Result is sent successfully';
        break;
      case ProcessStatus.error:
        headerText = 'Something went wrong while trying to calculate the path';
        break;
    }
    return Text(headerText, textAlign: TextAlign.center);
  }

  Widget _buildProgressIndicator(double progress) => Center(
        child: Padding(
          padding: edgeInsertsAll16,
          child: SizedBox(
            height: 100,
            width: 100,
            child: CircularProgressIndicator(
              value: progress,
            ),
          ),
        ),
      );

  Widget _buildSendButton(BuildContext context, ProcessStatus status) => SizedBox(
        width: double.infinity,
        child: ElevatedButton(
          onPressed: status == ProcessStatus.success || status == ProcessStatus.error
              ? () {
                  BlocProvider.of<ProcessBloc>(context).add(SendResultEvent(url));
                }
              : null,
          child: status == ProcessStatus.loading ? loadingIndicator : const Text('Send results to server'),
        ),
      );
}
