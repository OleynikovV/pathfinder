import 'package:flutter/material.dart';

const Widget loadingIndicator = Center(
  child: CircularProgressIndicator(),
);
