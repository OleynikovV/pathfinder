import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../../data/model/result.dart';
import '../../../util/resources/edge_inserts.dart';

class ResultListPage extends StatelessWidget {
  final List<Result> resultList;

  const ResultListPage(this.resultList, {super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Result list screen'),
      ),
      body: SafeArea(
        child: Padding(
          padding: edgeInsertsAll8,
          child: ListView.separated(
            physics: const AlwaysScrollableScrollPhysics(
              parent: BouncingScrollPhysics(),
            ),
            itemCount: resultList.length,
            itemBuilder: (context, index) {
              final result = resultList[index];
              return InkWell(
                splashColor: Colors.white70,
                onTap: () {
                  context.push(
                    '/field_fetch/result_list/preview',
                    extra: {'result': result},
                  );
                },
                child: Padding(
                  padding: edgeInsertsAll16,
                  child: Center(
                    child: Text(result.path),
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) => const Divider(),
          ),
        ),
      ),
    );
  }
}
