import 'dart:math';

import 'package:flutter/material.dart';

import '../../../data/model/node.dart';
import '../../../data/model/result.dart';
import '../../../util/resources/edge_inserts.dart';
import '../../../util/resources/grid_colors.dart';

class PreviewPage extends StatelessWidget {
  final Result result;

  const PreviewPage(this.result, {super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Preview screen'),
      ),
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _buildGrid(),
            _buildPathText(),
          ],
        ),
      ),
    );
  }

  Widget _buildGrid() {
    final grid = result.field.grid;
    final steps = result.steps;

    return GridView.count(
      shrinkWrap: true,
      crossAxisCount: grid.length,
      children: List.generate(
        grid.length * grid.length,
        (index) {
          final rowIndex = index ~/ grid.length;
          final columnIndex = index % grid.length;
          final nodeValue = grid[rowIndex][columnIndex];
          final isObstacle = nodeValue == 'X';

          Color nodeColor;
          final node = Node(Point(rowIndex, columnIndex));
          if (steps.isNotEmpty && steps.contains(node)) {
            if (steps.first == node) {
              nodeColor = startNodeColor;
            } else if (steps.last == node) {
              nodeColor = endNodeColor;
            } else {
              nodeColor = pathNodeColor;
            }
          } else {
            nodeColor = isObstacle ? obstacleNodeColor : emptyNodeColor;
          }

          return _buildNode(nodeColor, columnIndex, rowIndex, isObstacle);
        },
      ),
    );
  }

  Widget _buildNode(Color nodeColor, int columnIndex, int rowIndex, bool isObstacle) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border.all(),
        color: nodeColor,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            '($columnIndex, $rowIndex)',
            style: TextStyle(
              color: isObstacle ? Colors.white : null,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPathText() => Padding(
        padding: edgeInsertsAll8,
        child: Text(
          result.path,
          textAlign: TextAlign.center,
        ),
      );
}
