import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../../util/resources/edge_inserts.dart';
import '../../widgets/expanded_box.dart';
import '../../widgets/loading_indicator.dart';
import '../bloc/field_fetch_bloc.dart';

class FieldFetchPage extends StatefulWidget {
  const FieldFetchPage({super.key});

  @override
  FieldFetchPageState createState() => FieldFetchPageState();
}

class FieldFetchPageState extends State<FieldFetchPage> {
  final _urlController = TextEditingController();

  @override
  void dispose() {
    _urlController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home screen'),
      ),
      body: SafeArea(
        child: Padding(
          padding: edgeInsertsAll16,
          child: BlocConsumer<FieldFetchBloc, FieldFetchState>(
            listener: (context, state) {
              if (state.status == FieldFetchStatus.error) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(state.errorMessage),
                    backgroundColor: Colors.red,
                  ),
                );
              }
              if (state.status == FieldFetchStatus.success) {
                context.go('/field_fetch/process', extra: {
                  'url': state.url,
                  'fieldList': state.fieldList,
                });
              }
            },
            builder: (context, state) {
              if (state.status == FieldFetchStatus.loading) {
                return loadingIndicator;
              }
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Set valid API base URL in order to continue'),
                  ListTile(
                    leading: const Icon(Icons.compare_arrows_rounded),
                    title: TextField(
                      controller: _urlController,
                    ),
                  ),
                  expandedBox,
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: () {
                        BlocProvider.of<FieldFetchBloc>(context).add(FetchEvent(_urlController.text));
                      },
                      child: const Text('Start counting process'),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
