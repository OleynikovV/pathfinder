part of 'field_fetch_bloc.dart';

enum FieldFetchStatus { initial, loading, success, error }

class FieldFetchState extends Equatable {
  final FieldFetchStatus status;
  final String errorMessage;

  final String url;
  final List<Field> fieldList;

  const FieldFetchState({
    this.status = FieldFetchStatus.initial,
    this.errorMessage = 'Something went wrong',
    this.url = '',
    this.fieldList = const [],
  });

  FieldFetchState copyWith({
    FieldFetchStatus? status,
    String? errorMessage,
    String? url,
    List<Field>? fieldList,
  }) =>
      FieldFetchState(
        status: status ?? this.status,
        errorMessage: errorMessage ?? this.errorMessage,
        url: url ?? this.url,
        fieldList: fieldList ?? this.fieldList,
      );

  @override
  List<Object> get props => [status, errorMessage, url, fieldList];
}
