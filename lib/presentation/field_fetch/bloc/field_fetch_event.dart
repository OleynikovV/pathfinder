part of 'field_fetch_bloc.dart';

sealed class FieldFetchEvent extends Equatable {
  const FieldFetchEvent();

  @override
  List<Object> get props => [];
}

class FetchEvent extends FieldFetchEvent {
  final String url;

  const FetchEvent(this.url);

  @override
  List<Object> get props => [url];
}
