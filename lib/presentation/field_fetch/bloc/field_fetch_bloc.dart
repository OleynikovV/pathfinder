import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/model/field.dart';
import '../repository/field_fetcher.dart';

part 'field_fetch_event.dart';
part 'field_fetch_state.dart';

class FieldFetchBloc extends Bloc<FieldFetchEvent, FieldFetchState> {
  final FieldFetcher _fieldFetcher;

  FieldFetchBloc(this._fieldFetcher) : super(const FieldFetchState()) {
    on<FetchEvent>(_onFetch);
  }

  _onFetch(FetchEvent event, Emitter<FieldFetchState> emit) async {
    if (Uri.parse(event.url).isAbsolute) {
      emit(state.copyWith(
        status: FieldFetchStatus.loading,
        url: event.url,
      ));

      try {
        final fieldList = await _fieldFetcher.fetchField(event.url);

        if (fieldList.isNotEmpty) {
          emit(state.copyWith(
            status: FieldFetchStatus.success,
            fieldList: fieldList,
          ));
        } else {
          emit(state.copyWith(
            status: FieldFetchStatus.error,
            errorMessage: 'Failed to fetch the list',
          ));
        }
      } catch (e) {
        emit(state.copyWith(
          status: FieldFetchStatus.error,
          errorMessage: 'Failed to fetch the list',
        ));
      }
    } else {
      emit(state.copyWith(
        status: FieldFetchStatus.error,
        errorMessage: 'Make sure you enter the URL correctly',
      ));
    }
  }
}
