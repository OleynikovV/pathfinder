import '../../../data/model/field.dart';
import '../../../data/repository/http_client.dart';

class FieldFetcher {
  final HttpClient _client;

  FieldFetcher(this._client);

  Future<List<Field>> fetchField(String url) async {
    try {
      final responseData = await _client.executeGetRequest(url);

      if (!responseData['error']) {
        return (responseData['data'] as List).map((item) => Field.fromJson(item)).toList();
      } else {
        throw Exception(responseData['message']);
      }
    } catch (e) {
      rethrow;
    }
  }
}
