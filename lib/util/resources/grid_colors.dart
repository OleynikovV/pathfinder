import 'package:flutter/material.dart';

const startNodeColor = Color(0xFF64ffda);

const endNodeColor = Color(0xFF009688);

const obstacleNodeColor = Color(0xFF000000);

const pathNodeColor = Color(0xFF4CAF50);

const emptyNodeColor = Color(0xFFFFFFFF);
