import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

import '../data/repository/dio_client.dart';
import '../data/repository/http_client.dart';
import '../data/services/a_star_path_finder.dart';
import '../data/services/path_finder.dart';
import '../presentation/field_fetch/field_fetch.dart';
import '../presentation/progress/process.dart';

final getIt = GetIt.instance;
void setup() {
  //
  getIt.registerLazySingleton<HttpClient>(
    () => DioClient(getIt()),
  );

  getIt.registerFactory<PathFinder>(
    () => AStarPathFinder(),
  );

  getIt.registerLazySingleton<FieldFetcher>(
    () => FieldFetcher(getIt()),
  );

  getIt.registerLazySingleton<ResultSender>(
    () => ResultSender(getIt()),
  );

  //
  getIt.registerFactory<FieldFetchBloc>(
    () => FieldFetchBloc(getIt()),
  );

  getIt.registerFactory<ProcessBloc>(
    () => ProcessBloc(getIt(), getIt()),
  );

  //
  getIt.registerLazySingleton<Dio>(
    () => Dio(),
  );
}
