import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:webspark_app/util/locator.dart';

import '../presentation/field_fetch/field_fetch.dart';
import '../presentation/preview/preview.dart';
import '../presentation/progress/process.dart';
import '../presentation/result_list/result_list.dart';

final routerConfig = GoRouter(
  initialLocation: '/field_fetch',
  routes: [
    GoRoute(
      path: '/field_fetch',
      builder: (context, state) => BlocProvider(
        create: (context) => getIt.get<FieldFetchBloc>(),
        child: const FieldFetchPage(),
      ),
      routes: [
        GoRoute(
          path: 'process',
          builder: (context, state) {
            final extra = state.extra as Map<String, dynamic>;
            return BlocProvider(
              create: (context) => getIt.get<ProcessBloc>()..add(CalculateResultEvent(extra['fieldList'])),
              child: ProcessPage(extra['url'], extra['fieldList']),
            );
          },
        ),
        GoRoute(
          path: 'result_list',
          builder: (context, state) {
            final extra = state.extra as Map<String, dynamic>;
            return ResultListPage(extra['resultList']);
          },
          routes: [
            GoRoute(
              path: 'preview',
              builder: (context, state) {
                final extra = state.extra as Map<String, dynamic>;
                return PreviewPage(extra['result']);
              },
            ),
          ],
        ),
      ],
    ),
  ],
);
