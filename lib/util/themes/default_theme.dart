import 'package:flutter/material.dart';
import 'package:webspark_app/util/resources/edge_inserts.dart';

final defaultTheme = ThemeData(
  useMaterial3: true,
  appBarTheme: const AppBarTheme(
    backgroundColor: Colors.blue,
    foregroundColor: Colors.white,
    shadowColor: Colors.black,
    elevation: 5,
    titleTextStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.normal),
  ),
  textTheme: const TextTheme(
    bodyMedium: TextStyle(fontSize: 18),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      padding: edgeInsertsAll16,
      textStyle: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      foregroundColor: Colors.black,
      backgroundColor: Colors.lightBlue[300],
      side: const BorderSide(
        color: Colors.blue,
        width: 2,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
    ),
  ),
  progressIndicatorTheme: const ProgressIndicatorThemeData(color: Colors.blue),
);
